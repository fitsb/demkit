# Benvenuto su DEMKIT
> Kit completo per lo sviluppo di DEM

Questi moduli sono creati seguendo la metodologia di Atomic Design.

## Sviluppo
Al primo avvio si scaricano le dipendenze
```bash
npm install
```
Si installa a livello globale live-server
```bash
npm install -g live-server
```
E successivamente 
```bash
npm run dev
```