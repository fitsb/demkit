/*
 Copyright 2014 Google Inc. All rights reserved.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 
 
 
 Questo sito utilizza cookie per garantire la funzionalità del sito e per tenere conto delle vostre scelte di navigazione in modo da offrirvi la migliore esperienza sul nostro sito.
 Se vuoi saperne di più o negare il consenso a tutti o ad alcuni cookie
clicca qui. Continuando a navigare sul sito, accetti di utilizzare i cookies.
Per proseguire conferma di aver preso visione di questo avviso cliccando sul tasto qui sotto.

Utilizziamo i cookies per garantire la funzionalità del sito e per tenere conto delle vostre scelte di navigazione in modo da offrirvi la migliore esperienza sul nostro sito.
Inoltre ci riserviamo di utilizzare cookies di parti terze. Per saperne di più e sapere come negare il consenso a tutti o ad alcuni cookie consulta la nostra Cookie Policy.
Continuando a navigare sul sito, accetti di utilizzare i cookies.
Per proseguire conferma di aver preso visione di questo avviso cliccando sul tasto qui sotto.



<script src="/cookiechoices.js"></script>
<script>
  document.addEventListener('DOMContentLoaded', function(event) {
    cookieChoices.showCookieConsentBar('Your message for visitors here',
      'close message', 'learn more', 'http://example.com');
  });
</script>
<script>
  document.addEventListener('DOMContentLoaded', function(event) {
    cookieChoices.showCookieConsentBar('Questo sito utilizza cookie per garantire la funzionalità del sito e per tenere conto delle vostre scelte di navigazione in modo da offrirvi la migliore esperienza sul nostro sito.<br>
	Se vuoi saperne di più o negare il consenso a tutti o ad alcuni cookie <a href="/cookie-policy">clicca qui</a>.<br>Continuando a navigare sul sito, accetti di utilizzare i cookies.<br>
Per proseguire conferma di aver preso visione di questo avviso cliccando sul tasto qui sotto.',
      'Accetto', '', '');
  });
</script>

Questo sito utilizza cookie per garantire la funzionalità del sito e per tenere conto delle vostre scelte di navigazione in modo da offrirvi la migliore esperienza sul nostro sito.
 Se vuoi saperne di più o negare il consenso a tutti o ad alcuni cookie clicca qui. Continuando a navigare sul sito, accetti di utilizzare i cookies.
Per proseguire conferma di aver preso visione di questo avviso cliccando sul tasto qui sotto.
 */

(function(window) {

  if (!!window.cookieChoices) {
    return window.cookieChoices;
  }

  var document = window.document;
  // IE8 does not support textContent, so we should fallback to innerText.
  var supportsTextContent = 'textContent' in document.body;

  var cookieChoices = (function() {

    var cookieName = 'displayCookieConsent';
    var cookieConsentId = 'cookieChoiceInfo';
    var dismissLinkId = 'cookieChoiceDismiss';
	
    function _createHeaderElement(cookieText, dismissText, linkText, linkHref) {
      var butterBarStyles = 'position:fixed;width:100%;background-color:#eee;' +
          'margin:0; left:0; top:0;padding:4px;z-index:1000;text-align:center;font-size:12px;padding:30px 0;';

      var cookieConsentElement = document.createElement('div');
      cookieConsentElement.id = cookieConsentId;
      cookieConsentElement.style.cssText = butterBarStyles;
      cookieConsentElement.appendChild(_createConsentText(cookieText));

      if (!!linkText && !!linkHref) {
        cookieConsentElement.appendChild(_createInformationLink(linkText, linkHref));
      }
      cookieConsentElement.appendChild(_createDismissLink(dismissText));
      return cookieConsentElement;
    }

    function _createDialogElement(cookieText, dismissText, linkText, linkHref) {
      var glassStyle = 'position:fixed;width:100%;height:100%;z-index:999;' +
          'top:0;left:0;opacity:0.5;filter:alpha(opacity=50);' +
          'background-color:#ccc;font-size:12px;';
      var dialogStyle = 'z-index:1000;position:fixed;left:50%;top:50%;font-size:12px;';
      var contentStyle = 'position:relative;left:-50%;margin-top:-25%;' +
          'background-color:#fff;padding:20px;box-shadow:4px 4px 25px #888;font-size:12px;';

      var cookieConsentElement = document.createElement('div');
      cookieConsentElement.id = cookieConsentId;

      var glassPanel = document.createElement('div');
      glassPanel.style.cssText = glassStyle;

      var content = document.createElement('div');
      content.style.cssText = contentStyle;

      var dialog = document.createElement('div');
      dialog.style.cssText = dialogStyle;

      var dismissLink = _createDismissLink(dismissText);
      dismissLink.style.display = 'block';
      dismissLink.style.textAlign = 'right';
      dismissLink.style.marginTop = '8px';

      content.appendChild(_createConsentText(cookieText));
      if (!!linkText && !!linkHref) {
        content.appendChild(_createInformationLink(linkText, linkHref));
      }
      content.appendChild(dismissLink);
      dialog.appendChild(content);
      cookieConsentElement.appendChild(glassPanel);
      cookieConsentElement.appendChild(dialog);
      return cookieConsentElement;
    }

    function _setElementText(element, text) {
      
	  if (supportsTextContent) {
        element.textContent = text;
      } else {
        element.innerText = text;
      }
	  element.innerHTML = text;
    }

    function _createConsentText(cookieText) {
      var consentText = document.createElement('span');
      _setElementText(consentText, cookieText);
      return consentText;
    }

    function _createDismissLink(dismissText) {
      var dismissLink = document.createElement('a');
      _setElementText(dismissLink, dismissText);
      dismissLink.id = dismissLinkId;
      dismissLink.href = '#';
      dismissLink.style.marginLeft = '24px';
      return dismissLink;
    }

    function _createInformationLink(linkText, linkHref) {
      var infoLink = document.createElement('a');
      _setElementText(infoLink, linkText);
      infoLink.href = linkHref;
      infoLink.target = '_blank';
      infoLink.style.marginLeft = '8px';
      return infoLink;
    }

    function _dismissLinkClick() {
      _saveUserPreference();
      _removeCookieConsent();
      return false;
    }

    function _showCookieConsent(cookieText, dismissText, linkText, linkHref, isDialog) {
		
      if (_shouldDisplayConsent()) {
        _removeCookieConsent();
        var consentElement = (isDialog) ?
            _createDialogElement(cookieText, dismissText, linkText, linkHref) :
            _createHeaderElement(cookieText, dismissText, linkText, linkHref);
        var fragment = document.createDocumentFragment();
        fragment.appendChild(consentElement);
        document.body.appendChild(fragment.cloneNode(true));
        document.getElementById(dismissLinkId).onclick = _dismissLinkClick;
        window.onscroll = _dismissLinkClick;
      }else{
		  
	  }
    }

    function showCookieConsentBar(cookieText, dismissText, linkText, linkHref) {
		if(document.getElementById('cookieChoiceInfo')=="undefined"){
			_showCookieConsent(cookieText, dismissText, linkText, linkHref, false);
		}else{
			
			 if(!_shouldDisplayConsent()){
			  	_removeCookieConsent();
			 }else{
			 	document.getElementById(dismissLinkId).onclick = _dismissLinkClick;
			 	window.onscroll = _dismissLinkClick;
				el=document.getElementById('cookieChoiceInfo');
				el.style.display = "block";
			 }
		}
    }

    function showCookieConsentDialog(cookieText, dismissText, linkText, linkHref) {
      _showCookieConsent(cookieText, dismissText, linkText, linkHref, true);
    }

    function _removeCookieConsent() {
      var cookieChoiceElement = document.getElementById(cookieConsentId);
      if (cookieChoiceElement != null) {
        cookieChoiceElement.parentNode.removeChild(cookieChoiceElement);
      }
    }

    function _saveUserPreference() {
      // Set the cookie expiry to one year after today.
      var expiryDate = new Date();
      expiryDate.setFullYear(expiryDate.getFullYear() + 1);
      document.cookie = cookieName + '=y; expires=' + expiryDate.toGMTString() + ';path=/';
    }

    function _shouldDisplayConsent() {
      // Display the header only if the cookie has not been set.
	  
	  
      return !document.cookie.match(new RegExp(cookieName + '=([^;]+)'));
    }

    var exports = {};
    exports.showCookieConsentBar = showCookieConsentBar;
    exports.showCookieConsentDialog = showCookieConsentDialog;
    return exports;
  })();

  window.cookieChoices = cookieChoices;
  return cookieChoices;
})(this);
